package com.Model;

import java.sql.Date;

public class Roles {
private long Roleid;
private String rolename;
private Date date;
private boolean isActive;

public long getRoleid() {
	return Roleid;
}
public void setRoleid(long roleid) {
	Roleid = roleid;
}
public String getRolename() {
	return rolename;
}
public void setRolename(String rolename) {
	this.rolename = rolename;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public boolean isActive() {
	return isActive;
}
public void setActive(boolean isActive) {
	this.isActive = isActive;
}


}
