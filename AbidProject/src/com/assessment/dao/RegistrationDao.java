package com.assessment.dao;

import org.postgresql.util.PSQLException;

import com.assessments.models.User;

public interface RegistrationDao {

	boolean register1(User user) throws PSQLException ;

	boolean register(User user);

	
	  
}

