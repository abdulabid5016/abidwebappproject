package com.assessment.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.assessments.models.User;

public interface LoginService {

	User Login(HttpServletRequest req, HttpServletResponse resp);

}
