package com.assessment.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.postgresql.util.PSQLException;

import com.assessment.dao.RegistrationDao;
import com.assessment.dao.impl.RegistrationDaoimpl;
import com.assessment.service.RegisterService;
import com.assessments.models.User;

public class RegisterServiceImpl implements RegisterService {
	private RegistrationDao registerDao=new RegistrationDaoimpl();
	
	public boolean register(HttpServletRequest req, HttpServletResponse resp) {
		User user=new User();
		
		String email=req.getParameter("email");
		String password=req.getParameter("password");
		String name=req.getParameter("name");
		String mobile=req.getParameter("mobile");
		user.setEmail(email);
		user.setPassword(password);
		user.setName(name);
		user.setMobile(mobile);
		
		try {
			return registerDao.register1(user);
		} catch (PSQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	
	}
}
